#include <stdio.h>
#include <limits.h>
#include <malloc.h>
#include <windows.h>
#include <stdlib.h>
#include <string.h>
#include <conio.h>

#define Next(P) (P)->next
#define NamaPengunjung(P) (P)->nama_pengunjung
#define Date(P) (P)->tgl_order.date
#define Month(P) (P)->tgl_order.month
#define Year(P) (P)->tgl_order.year
#define Hour(P) (P)->tgl_order.hour
#define Min(P) (P)->tgl_order.min
#define IdOrder(P) (P)->id_order
#define IdBooking(P) (P)->id_booking
#define First(L) (L).first


#ifndef header_H

//string
typedef char * String;

//date
typedef struct {
 	int date;
 	int month;
 	int year;
 	int hour;
 	int min;
} O_Date;

//t_order
typedef struct O_Order * P_Order;
typedef struct O_Order{
 	int id_order;
	O_Date tgl_order;
	int id_booking;
	String nama_pengunjung;
	P_Order next;
} O_Order;
typedef struct{
	P_Order first;
} L_Order;

//file.c
char * newString(int size);
void rewritefile(int id_file);
String readFile(int id_file);
void s(String c);

//list_order.c
P_Order allocOrder();
void insertOrder(O_Order X);
void selectOrder();
void addOrder();
void createListOrder(L_Order * L);
int syncListOrder(L_Order * L);


#endif
