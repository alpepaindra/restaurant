#include "header.h"

P_Order allocOrder(){
	P_Order P = (P_Order) malloc(sizeof(O_Order));
	NamaPengunjung(P) = newString(60);
	Next(P)  = NULL;
	return P;
}

void addOrder(){
	s("cls");
	O_Order order;
	order.nama_pengunjung = newString(60);
	printf("Nama Pengunjung : ");
	scanf("%s",order.nama_pengunjung);
	printf("Tanggal Order (dd/MM/yyyy hh:mm) : ");
	scanf("%d/%d/%d %d:%d",&order.tgl_order.date,&order.tgl_order.month,&order.tgl_order.year,&order.tgl_order.hour,&order.tgl_order.min);
	printf("Id Booking : ");
	scanf("%d",&order.id_booking);
	printf("Id Order : ");
	scanf("%d",&order.id_order);
	insertOrder(order);
}

void selectOrder(){
	L_Order list_order;
	printf("Jumlah Data = %d\n",syncListOrder(&list_order));
	
	P_Order P = First(list_order);
	while(P != NULL){
		printf("Id Order : %d\nId Booking : %d\nTanggal Order : %d/%d/%d %d:%d\nNama Pengunjung : %s\n\n",
			IdOrder(P),IdBooking(P),Date(P),Month(P),Year(P),Hour(P),Min(P),NamaPengunjung(P));
		P = Next(P);
	}
}

void insertOrder(O_Order X){
	
 	String data = newString(1000);

 	snprintf(data, sizeof(char)*1000, "%d;%d;%d;%d;%d;%d;%d;%s;|\n",X.id_order,X.tgl_order.date,X.tgl_order.month,X.tgl_order.year,X.tgl_order.hour,X.tgl_order.min,X.id_booking,X.nama_pengunjung);
	FILE *fptr;
 	fptr=fopen("t_order.txt", "a");
 	if(fptr==NULL){
 		printf("Tidak bisa membuka t_order.txt!");
 	}else{
		fprintf(fptr, "%s", data);
		fclose(fptr);
	}
}

void createListOrder(L_Order * L){
	First((*L)) = NULL;
}

int syncListOrder(L_Order * L){
	
	createListOrder(L);
	
	P_Order P,PData;
	PData = First((*L));
	String c = newString(1000);
	char tmp;
	int count=0;
	
	FILE *fptr;
	if ((fptr=fopen("t_order.txt","r"))==NULL){
		 printf("Tidak bisa membuka t_order.txt!");
	}else{
		P = allocOrder();
		int j=0;
		do {
			tmp = fgetc(fptr);
			if(tmp==';'){
				switch(j){
					case 0 :
						IdOrder(P) = atoi(c);
						break;
					case 1 :
						Date(P) = atoi(c);
						break;
					case 2 :
						Month(P) = atoi(c);
						break;
					case 3 :
						Year(P) = atoi(c);
						break;
					case 4 :
						Hour(P) = atoi(c);
						break;
					case 5 :
						Min(P) = atoi(c);
						break;
					case 6 :
						IdBooking(P) = atoi(c);
						break;
					case 7 :
						strcpy(NamaPengunjung(P),c);
						break;
				}
			j++;
			c[0]=NULL;		   
			}
			else if (tmp=='|'){
				count++;
				j=0;
				if(PData==NULL){
					First((*L)) = P;
					PData = P;
				}
				else{
					Next(PData) = P;
					PData = Next(PData);
				}
				P = allocOrder();
				c[0]=NULL;
			}else{
				snprintf(c,1000,"%s%c",c,tmp);
			}
		}while(!feof(fptr));
		P_Order tmp1 = First((*L));
		fclose(fptr);
	}
	return count;
}
